# PAM auth via GPG

Instead of checking the password or shadow file, this PAM module tries to encrypt and decrypt a file using a specific GPG key.  This is useful if you have a pin-secured key on a smartcard, since it requires the physical device and reduces the number of creds you have to manage.

This is based on https://github.com/0x17de/pam_gpg .

## Installation and usage

You need to have Rust and Cargo installed.

1. Run `cargo build --release`
2. Test it with `./target/release/pamgpg` - it should print `Pass`.
3. Copy `target/release/libpamgpg.so` to your PAM module directory, changing ownership appropriately.  On Debian this is `/lib/x86_64-linux-gnu/security`.
4. Edit `/etc/pam.d/common-auth` and add:
   ```
   auth sufficient libpamgpg.so
   ```
   before the line iwht `pam_unix.so`.
   **Keep the file open** so you can undo if things don't work!
5. Export each user's keys to `/etc/pamgpg/username.gpg` (replacing `username`).

   If using a smart card, you only need to export the public key: `gpg --export userkeyname > /etc/pamgpg/username.gpg`, replacing `userkeyname` and `username`.
6. Test your changes by opening a new shell and running `sudo echo hi` -- if this doesn't work as expected, revert the changes in 4 before doing further testing.

