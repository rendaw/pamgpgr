use std::process::Command;

use log::LevelFilter::Debug;
use log::{set_logger, set_max_level, Metadata, Record};
use pamgpg::{authenticate, Inv};
use std::str::from_utf8;

struct DebugLogger;

impl log::Log for DebugLogger {
    fn enabled(&self, _metadata: &Metadata) -> bool {
        true
    }

    fn log(&self, record: &Record) {
        println!("{:?}", record)
    }

    fn flush(&self) {}
}

const DEBUG_LOGGER: DebugLogger = DebugLogger;

fn main() {
    set_logger(&DEBUG_LOGGER).unwrap();
    set_max_level(Debug);
    match Command::new("whoami")
        .output()
        .expect("need whoami")
        .invm(&|x| {
            from_utf8(&x.stdout)
                .map(|v| v.to_string())
                .expect("bad whoami output")
        }).invm(&|x| authenticate(x.trim_end().as_bytes()))
    {
        Ok(_) => println!("Ok"),
        Err(e) => {
            e.log();
            println!("Fail")
        }
    }
    .into()
}
