use std::ffi::{CStr, OsStr};
use std::io::Write;
use std::os::raw::{c_char, c_int, c_void};
use std::os::unix::ffi::OsStrExt;
use std::path::Path;
use std::process::{Child, Command, Stdio};
use std::ptr::null;

use log::{info, warn, LevelFilter, Metadata, Record};
use pam_sys::raw::pam_get_item;
use pam_sys::PamItemType::USER;
use pam_sys::{PamHandle, PamReturnCode};
use rand::{thread_rng, Rng};
use syslog::{BasicLogger, Facility, Formatter3164};

pub enum E {
    PamhGetUser,
    GpgSpawn(&'static str, std::io::Error),
    GpgWrite(&'static str, std::io::Error),
    GpgWait(&'static str, std::io::Error),
    GpgRet(&'static str, Option<i32>),
}

impl E {
    pub fn log(&self) -> () {
        match self {
            E::PamhGetUser => warn!("Failed to get USER from pamh"),
            E::GpgSpawn(path, e) => warn!("Failed to spawn GPG: {} / {:?}", path, e),
            E::GpgWrite(mode, e) => warn!("Failed to write to GPG ({}): {:?}", mode, e),
            E::GpgWait(mode, e) => warn!("Failed to wait for GPG process exit ({}): {:?}", mode, e),
            E::GpgRet(mode, e) => warn!("GPG exited with code ({}): {:?}", mode, e),
        }
    }
}

pub trait Inv<T> {
    fn inv(self, apply: &dyn Fn(&T) -> ()) -> T;
    fn invr_mut<R, E>(self, apply: &dyn Fn(&mut T) -> Result<R, E>) -> Result<T, E>;
    fn inve<E>(self, apply: &dyn Fn(&T) -> Result<(), E>) -> Result<T, E>;
    fn invm<V>(self, apply: &dyn Fn(T) -> V) -> V;
}

impl<T> Inv<T> for T {
    fn inv(self, apply: &dyn Fn(&T) -> ()) -> T {
        apply(&self);
        self
    }

    fn invr_mut<R, E>(mut self, apply: &dyn Fn(&mut T) -> Result<R, E>) -> Result<T, E> {
        apply(&mut self)?;
        Ok(self)
    }

    fn inve<E>(self, apply: &dyn Fn(&T) -> Result<(), E>) -> Result<T, E> {
        match apply(&self) {
            Err(e) => return Err(e),
            _ => (),
        };
        Ok(self)
    }

    fn invm<V>(self, apply: &dyn Fn(T) -> V) -> V {
        apply(self)
    }
}

pub fn get_user(pamh: *const PamHandle) -> Result<Vec<u8>, E> {
    Ok(unsafe {
        let mut out: *const c_void = null();
        let res = pam_get_item(pamh, USER as c_int, &mut out);
        if res != PamReturnCode::SUCCESS as c_int {
            return Err(E::PamhGetUser);
        }
        CStr::from_ptr(out as *const c_char).to_bytes()
    }
    .into())
}

fn write_all_close(p: &mut Child, b: &[u8]) -> Result<(), std::io::Error> {
    let mut stdin = p.stdin.take().unwrap();
    stdin.write_all(b)?;
    drop(stdin);
    Ok(())
}

pub fn authenticate(user: &[u8]) -> Result<(), E> {
    let suffix = b".gpg";
    let mut key = vec![0; user.len() + suffix.len()];
    key[0..user.len()].clone_from_slice(user);
    key[user.len()..].clone_from_slice(suffix);
    let keyring = Path::new("/etc/pamgpg/").join(OsStr::from_bytes(&key));
    let gpg = "/usr/bin/gpg";
    let signature = Command::new(&gpg)
        .arg("--no-default-keyring")
        .arg("--keyring")
        .arg(&keyring)
        .arg("--sign")
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn()
        .map_err(|e| E::GpgSpawn(gpg, e))?
        .invr_mut(&|p| write_all_close(p, &thread_rng().gen::<[u8; 32]>()))
        .map_err(|e| E::GpgWrite("sign", e))?
        .wait_with_output()
        .map_err(|e| E::GpgWait("sign", e))?
        .inv(&|o| {
            let b = &o.stderr;
            if b.len() == 0 {
                return;
            }
            warn!("{}", String::from_utf8_lossy(b));
        })
        .inve(&|p| {
            if p.status.success() {
                Ok(())
            } else {
                Err(E::GpgRet("sign", p.status.code()))
            }
        })?
        .invm(&|p| p.stdout);
    Command::new(&gpg)
        .arg("--no-default-keyring")
        .arg("--keyring")
        .arg(&keyring)
        .arg("--verify")
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn()
        .map_err(|e| E::GpgSpawn(gpg, e))?
        .invr_mut(&|p| write_all_close(p, &signature))
        .map_err(|e| E::GpgWrite("verify", e))?
        .wait_with_output()
        .map_err(|e| E::GpgWait("verify", e))?
        .inv(&|o| {
            let b = &o.stderr;
            if b.len() == 0 {
                return;
            }
            warn!("{}", String::from_utf8_lossy(b));
        })
        .inv(&|o| {
            let b = &o.stdout;
            if b.len() == 0 {
                return;
            }
            info!("{}", String::from_utf8_lossy(b));
        })
        .status
        .invm(&|v| {
            if v.success() {
                Ok(())
            } else {
                Err(E::GpgRet("verify", v.code()))
            }
        })?;
    Ok(())
}

struct NoopLogger;

impl log::Log for NoopLogger {
    fn enabled(&self, _metadata: &Metadata) -> bool {
        false
    }

    fn log(&self, _record: &Record) {
        unimplemented!()
    }

    fn flush(&self) {}
}

#[no_mangle]
pub extern "C" fn pam_sm_authenticate(
    pamh: *const PamHandle,
    _flags: c_int,
    _argc: c_int,
    _argv: *const c_void,
) -> c_int {
    match syslog::unix(Formatter3164 {
        facility: Facility::LOG_AUTHPRIV,
        hostname: None,
        process: "pamgpg".to_string(),
        pid: 0,
    }) {
        Ok(x) => {
            log::set_boxed_logger(Box::new(BasicLogger::new(x))).unwrap();
            log::set_max_level(LevelFilter::Info);
        }
        Err(_) => {
            log::set_boxed_logger(Box::new(NoopLogger)).unwrap();
        }
    }
    (match &get_user(pamh).and_then(|v| authenticate(&v)) {
        Ok(_) => PamReturnCode::SUCCESS,
        Err(e) => {
            e.log();
            PamReturnCode::AUTH_ERR
        }
    }) as c_int
}
